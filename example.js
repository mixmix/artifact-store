const ArtifactStore = require('.')
const fs = require('fs')
const path = require('path')
const ram = require('random-access-memory')


async function main() {

    const key = ArtifactStore.artifactEncryptionKey()
    console.log(key.toString('hex'))
    const alice = new ArtifactStore('./alice')
    await alice.ready()
    const source = fs.createReadStream(path.join(path.resolve(__dirname), './README.md'))
    const target = alice.createWriteStream('./README.md', key)
    source.pipe(target)
    await new Promise((resolve, reject) => {
        alice.myDrive.on('update', resolve)
    })
    const sourceData = fs.readFileSync(path.join(path.resolve(__dirname), './README.md'), 'utf8')
    console.log('file exists on filesystem')
    let targetData = ''
    const readStream = await alice.getReadStream('./README.md', alice.driveAddress, key)
    for await (const data of readStream) {
        targetData += data.toString('utf8')
    }
    console.log(sourceData, targetData, 'source copied to target')
    // const dataSize = await alice.driveSize(alice.myDrive.key)
    // console.log('CONTENT DATA SIZE : ', dataSize)

}


main()


