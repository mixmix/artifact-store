# Draft Documentation for AhBox-CoHau

### README:

This is a small module that will plug into the back-end of Ahau for the storage and retrieval of 'artifacts': text, image, audio or video files.

Usage

API:

`const artifactStore = ArtifactStore(storagePath, options)`

Create a new `artifactStore` instance. `storagePath` can be either a string or a random-access-storage module.

`options` is an optional object which may include:
`bootstrap: [addresses]` - override the default DHT bootstrap nodes and use your own ones.  Taken an array of strings of the form `host:port`.

You can run your own DHT nodes using `https://github.com/hyperswarm/cli`, and then pass the host and port in here.

If you want to include the default nodes as well, they are:
`[
  'bootstrap1.hyperdht.org:49737',
  'bootstrap2.hyperdht.org:49737',
  'bootstrap3.hyperdht.org:49737'
]`

`await artifactStore.ready()`

Call after instantiating ArtifactStore, ensures user's own drive keys are correctly recorded. Returns a promise which resolves when artifactStore is fully initialised.

`const writeStream = artifactStore.createWriteStream(artifactFileName, artifactEncryptionKey, opts)`

Prepare to write a file to one of a user's own drives within their artifactStore. 
- `artifactFileName` should be a string.
- `artifactEncryptionKey` is a unique key required to encrypt the message being written. Should either be a string encoded in hex or a buffer.
- `opts` is an optional object that could contain the same options offered by fs.createWriteStream.

`await artifactStore.update()`

When writing to a hyperdrive, this method ensures the hyperdrive has updated and the file is accessible once written.

`const readStream = await artifactStore.getReadStream(artifactFileName, driveAddress, artifactEncryptionKey, opts = {})`

Prepare to read a file from a particular drive within an artifactStore. Unlike createReadStream, this returns a promise which, when resolved, will give a stream.
- `artifactFileName` should be a string.
- `driveAddress` should either be a string encoded in hex or a buffer.
- `artifactEncryptionKey` is a unique key required to decrypt the message being read. Should either be a string encoded in hex or a buffer.
- `opts` is an optional object. As with fs.createReadStream, it can contain `start: number` to indicate the position in a given file at which to start the readstream, and `end: number` to indicate the position in a given file at which to stop the readstream.

`artifactStore._replicate(isInitiator, opts)`

Internal method. Replicates one user's artifactStore to another's. `isInitiator` should be `true` for the artifactStore initiating the exchange, and `false` for the artifactStore responding. Returns a stream. Note: this is currently not used in this module, as corestore-networker does this internally, but is available in case it is desirable to have another method of replicating from peers.

`await artifactStore.remove(artifactFileName, driveAddress)`

Given an artifactFileName and its driveAddress, deletes the local copy of the file. Used to clear space on local device. NB this method does not check that there is another copy of the file existing on the network - ie. that this is not the only existing copy.

`await artifactStore.close()`

Gracefully closes connections to peers.

`const fileSize = await artifactStore.fileSize(artifactFileName, driveAddress)`

Given an artifactFileName and its driveAddress, returns the size in bytes of the file. Useful for telling the browser the size of a file so that it can effectively skip within the file while streaming.

`const driveSize = await artifactStore.driveSize(driveAddress)`

Given a driveAddress, returns the size in bytes of the locally stored copy of the drive.

`const artifactEncryptionKey = artifactStore.artifactEncryptionKey()`

Generates a unique key for encrypting each message that is added to a user's own artifactStore. Returns a buffer.

