const Hyperdrive = require('hyperdrive')
const Corestore = require('corestore')
const Networker = require('@corestore/networker')
const streamCipher = require('./encrypt')
const fs = require('fs')
const path = require('path')
const { promisify } = require('util')
const stat = promisify(fs.stat)
const raf = require('random-access-file')

module.exports = class ArtifactStore {

    constructor(storagePath, options = {}) {
        const storage = typeof storagePath === 'string'
            ? p => raf(path.join(storagePath, p))
            : storagePath
        this.storagePath = storage
        this.store = new Corestore(storage)
        this.networker = new Networker(this.store, options)
        this.otherDrives = new Map()
    }

    // Called after instantiating ArtifactStore, ensures user's own drive keys are correctly recorded. 
    async ready() {
        await this.store.ready()
        this.driveList = this.store.default({ valueEncoding: 'utf-8' })
        const self = this
        await this._setupMyDrive()
        // Make ourself addressable by announcing our discovery key
        await new Promise((resolve, reject) => {
            this.networker.once('opened', resolve)
            self.networker.configure(self.discoveryKey, { announce: true, lookup: false })
        })
        // Reconnect to known drives
        for await (const driveAddress of this.driveList.createReadStream()) {
            self._loadDrives(driveAddress)
        }
    }

    async _setupMyDrive() {
        const self = this
        self.myDrive = Hyperdrive(this.store.namespace('myDrive'), { sparse: false })
        await new Promise((resolve, reject) => {
            self.myDrive.on('ready', resolve)
        })
        self.discoveryKey = self.myDrive.discoveryKey
        self.driveAddress = self.myDrive.key
    }

    // on app start, loads drives that were previously connected to
    _loadDrives(driveAddress) {
        const drives = this.otherDrives
        if (drives.has(driveAddress.toString('hex'))) {
            return drives.get(driveAddress.toString('hex'))
        }
        const drive = Hyperdrive(this.store, driveAddress, { sparse: true })
        drives.set(driveAddress.toString('hex'), drive)
        if (!drive.discoveryKey) {
            drive.on('ready', () => { this._connect(drive) })
        }
        else { this._connect(drive) }
        return drive
    }

    async _getDrive(driveAddress) {
        const drives = this.otherDrives
        const self = this
        if (!Buffer.isBuffer(driveAddress)) {
            driveAddress = Buffer.from(driveAddress, 'hex')
        }
        if (Buffer.compare(driveAddress, self.driveAddress) === 0) {
            return self.myDrive
        }
        if (drives.has(driveAddress.toString('hex'))) {
            return drives.get(driveAddress.toString('hex'))
        }
        const drive = Hyperdrive(self.store, driveAddress, { sparse: true })
        drives.set(driveAddress.toString('hex'), drive)
        await new Promise((resolve, reject) => {
            self.driveList.append(driveAddress.toString('hex'), (err) => {
                if (err) return reject(err)
                resolve()
            })
        })
        if (!drive.discoveryKey) {
            await new Promise((resolve, reject) => { drive.on('ready', resolve) })
        }
        await self._connect(drive)
        return drive
    }

    // Connects to a peer through corestore networker.
    async _connect(drive) {
        await new Promise((resolve, reject) => {
            this.networker.once('peer-add', resolve)
            this.networker.once('flushed', resolve)
            this.networker.configure(drive.discoveryKey, { announce: false, lookup: true })
        })
    }

    // Prepares to add a file to user's own hyperdrive. Returns a writable stream.
    createWriteStream(artifactFileName, artifactEncryptionKey, opts) {
        const encryptStream = streamCipher(artifactEncryptionKey)
        const target = this.myDrive.createWriteStream(artifactFileName, opts).on('error', (err) => {
            encryptStream.destroy(err)
        })
        encryptStream.pipe(target)
        return encryptStream
    }

    // When writing to a hyperdrive, ensures the hyperdrive has updated and the file is accessible once written.
    async update(artifactFileName) {
        const self = this
        await new Promise((resolve, reject) => {
            self.myDrive.stat(artifactFileName, (err) => {
                if (!err) return resolve()
                self.myDrive.on('update', resolve)
            })
        })
    }

    // Prepares to read a file from a given hyperdrive in the corestore
    // opts // offset // {start: number} is the position in the file at which to start the readstream
    async getReadStream(artifactFileName, driveAddress, artifactEncryptionKey, opts = {}) {
        if (!Buffer.isBuffer(artifactEncryptionKey)) { artifactEncryptionKey = Buffer.from(artifactEncryptionKey, 'hex') }
        const encryptStream = streamCipher(artifactEncryptionKey, opts.start)
        const drive = await this._getDrive(driveAddress)
        const source = drive.createReadStream(artifactFileName, opts).on('error', (err) => {
            encryptStream.destroy(err)
        })
        source.pipe(encryptStream)
        return encryptStream
    }

    // Replicates one user's artifactStore to another's. Returns a stream.
    _replicate(isInitiator, opts) {
        return this.store._replicate(isInitiator, opts)
    }

    // Removes a file locally
    async remove(artifactFileName, driveAddress) {
        if (!Buffer.isBuffer(driveAddress)) {
            driveAddress = Buffer.from(driveAddress, 'hex')
        }
        const drive = await this._getDrive(driveAddress)
        await new Promise((resolve, reject) => {
            drive.clear(artifactFileName, (err) => {
                if (err) return reject(err)
                resolve()
            })
        })
    }

    async close() {
        await this.networker.close()
    }

    // Given an artifactFileName and its driveAddress, returns the size of the file
    async fileSize(artifactFileName, driveAddress) {
        const drive = await this._getDrive(driveAddress)
        const size = await new Promise((resolve, reject) => {
            drive.stat(artifactFileName, (err, stats) => {
                if (err) return reject(err)
                return resolve(stats.size)
            })
        })
        return size
    }

    // Given a drive address returns size of locally stored drive
    async driveSize(driveAddress) {
        const drive = await this._getDrive(driveAddress)
        const feed = await new Promise((resolve, reject) => {
            drive.getContent((err, feed) => {
                if (err) return reject(err)
                resolve(feed)
            })
        })
        const contentDiscKey = feed.discoveryKey
        // slice1/2 are where Hyperdrive file structure stores the data
        const slice1 = contentDiscKey.slice(0, 1).toString('hex')
        const slice2 = contentDiscKey.slice(1, 2).toString('hex')

        // fs.stat is an fs module method to give info about a file/system
        const stats = await stat(path.join(path.resolve(__dirname), this.storagePath, slice1, slice2, contentDiscKey.toString('hex'), 'data'))
        return stats.size
    }

    // Generates a unique key for encrypting each artifact added to the artifactStore. Returns a buffer.
    static artifactEncryptionKey() {
        return streamCipher.key()
    }

}

function logEvents(emitter, name) {
    const emit = emitter.emit
    name = name ? `(${name}) ` : ''
    emitter.emit = (...args) => {
        console.log(`\x1b[33m    ----${args[0]}\x1b[0m`)
        emit.apply(emitter, args)
    }
}
