const { describe } = require('tape-plus')
const fs = require('fs')
const path = require('path')
const streamCipher = require('../encrypt')
const { pipeline } = require('stream')
const hyperdrive = require('hyperdrive')
const util = require('./util')

const testFile = path.join(path.resolve(__dirname), '../package.json')
const testFileContents = fs.readFileSync(testFile, 'utf8')

describe('basic', (context) => {
  let testStorage

  context.beforeEach(() => {
    testStorage = util.makeTmpDir()
  })

  context.afterEach(async () => {
    await util.rmTmpDir(testStorage)
  })

  context('encrypt file and decrypt file', async (assert, next) => {
    const key = streamCipher.key()
    const encryptStream = streamCipher(key)

    const encryptedFile = path.join(testStorage, 'encryptedFileStored')

    await new Promise((resolve, reject) => {
      pipeline(
        fs.createReadStream(testFile),
        encryptStream,
        fs.createWriteStream(encryptedFile),
        (err) => {
          if (err) return reject(err)
          resolve()
        }
      )
    })

    const encryptedFileContents = fs.readFileSync(encryptedFile, 'utf8')
    assert.notEqual(encryptedFileContents, testFileContents, 'file successfully encrypted')

    const decryptStream = streamCipher(key)

    let decryptedContents = ''
    for await (const data of fs.createReadStream(encryptedFile)
      .pipe(decryptStream)) {
      decryptedContents += data.toString('utf8')
    }

    assert.equal(testFileContents, decryptedContents, 'file suscessfully decrypted')
    next()
  })

  context('put encrypted file in a hyperdrive', async (assert, next) => {
    const key = streamCipher.key()
    const encryptStream = streamCipher(key)
    const drive = hyperdrive(testStorage)

    const errorFromPipeline = await new Promise((resolve, reject) => {
      pipeline(
        fs.createReadStream(testFile),
        encryptStream,
        drive.createWriteStream('package.json'),
        (err) => {
          if (err) return reject(err)
          resolve()
        }
      )
    }).catch((err) => { return err })
    assert.error(errorFromPipeline, 'No error from pipeline')

    const decryptStream = drive.createReadStream('package.json')
      .pipe(streamCipher(key))

    let decryptedContents = ''
    for await (const data of decryptStream) {
      decryptedContents += data.toString('utf8')
    }
    assert.equal(testFileContents, decryptedContents, 'file successfully decrypted')
    next()
  })

  context('offset read', async (assert, next) => {
    const key = streamCipher.key()
    const encryptStream = streamCipher(key)

    const pipelineError = await new Promise((resolve, reject) => {
      pipeline(
        fs.createReadStream(testFile),
        encryptStream,
        fs.createWriteStream(testFile + '.enc'),
        (err) => {
          if (err) return reject(err)
          resolve()
        }
      )
    }).catch((err) => { return err })
    assert.error(pipelineError, 'No error from encrypt pipeline')

    const decryptStream = fs.createReadStream(testFile + '.enc', { start: 5 })
      .pipe(streamCipher(key, 5))

    let partialDecryptedContents = ''
    for await (const data of decryptStream) {
      partialDecryptedContents += data.toString('utf8')
    }

    assert.equal(partialDecryptedContents, testFileContents.slice(5), 'Portion of file successfully decrypted')
    next()
  })
})
