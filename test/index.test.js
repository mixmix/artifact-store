const { describe } = require('tape-plus')
const ArtifactStore = require('..')
const fs = require('fs')
const path = require('path')
const ram = require('random-access-memory')
const { promisify } = require('util')
const readFile = promisify(fs.readFile)
const util = require('util')
const localUtil = require('./util')
const stat = promisify(fs.stat)

describe('@artifact-store', (context) => {
    context('retrieve one own artifact', async (assert, next) => {
        const key = ArtifactStore.artifactEncryptionKey()
        const alice = new ArtifactStore(ram)
        await alice.ready()
        const source = fs.createReadStream(path.join(path.resolve(__dirname), '../README.md'))
        const target = alice.createWriteStream('./README.md', key)
        source.pipe(target)
        await alice.update('./README.md')
        const sourceData = await readFile(path.join(path.resolve(__dirname), '../README.md'), 'utf8').catch((err) => { return undefined })
        assert.ok(sourceData, 'file exists on filesystem')
        let targetData = ''
        const stream = await alice.getReadStream('./README.md', alice.driveAddress, key)
        for await (const data of stream) { targetData += data.toString('utf8') }
        assert.equal(sourceData, targetData, 'source copied to target')
        alice.close()
        next()
    })

    context('replicate one user corestore to another using networker', async (assert, next) => {
        const key = ArtifactStore.artifactEncryptionKey()
        const alice = new ArtifactStore(ram)
        await alice.ready()
        const source = fs.createReadStream(path.join(path.resolve(__dirname), '../README.md'))
        const target = alice.createWriteStream('./README.md', key)
        source.pipe(target)
        await alice.update('./README.md')
        const bob = new ArtifactStore(ram)
        await bob.ready()
        const streamAliceReadme = await bob.getReadStream('./README.md', alice.driveAddress, key)
        let aliceData = ''
        for await (const data of streamAliceReadme) { aliceData += data }
        const sourceData = await readFile(path.join(path.resolve(__dirname), '../README.md'), 'utf8').catch((err) => { return undefined })
        assert.ok(sourceData, 'aliceData on filesystem')
        assert.equal(aliceData, sourceData, 'bob has replicated alice\'s hyperdrive')
        alice.close()
        bob.close()
        next()
    })

    context('retrieve latter part of one own artifact', async (assert, next) => {
        const key = ArtifactStore.artifactEncryptionKey()
        const alice = new ArtifactStore(ram)
        await alice.ready()
        const source = fs.createReadStream(path.join(path.resolve(__dirname), '../README.md'))
        const target = alice.createWriteStream('./README.md', key)
        source.pipe(target)
        await alice.update('./README.md')
        let sourceData = ''
        const streamFS = fs.createReadStream(path.join(path.resolve(__dirname), '../README.md'), { start: 100 })
        for await (const data of streamFS) { sourceData += data.toString('utf8') }
        let targetData = ''
        const aliceStream = await alice.getReadStream('./README.md', alice.driveAddress, key, { start: 100 })
        for await (const data of aliceStream) { targetData += data.toString('utf8') }
        assert.equals(sourceData, targetData, 'source copied to target')
        alice.close()
        next()
    })

    context('remove one artifact from a store', async (assert, next) => {
        const testStorage = localUtil.makeTmpDir()
        const key = ArtifactStore.artifactEncryptionKey()
        const alice = new ArtifactStore(testStorage)
        await alice.ready()
        const source = fs.createReadStream(path.join(path.resolve(__dirname), '../README.md'))
        const target = alice.myDrive.createWriteStream('README.md')
        source.pipe(target)
        await alice.update('./README.md')
        // establish size of file
        const readmeStats = await stat(path.join(path.resolve(__dirname), '../README.md'))
        const readmeSize = readmeStats.size
        let sourceData = ''
        const streamFS = fs.createReadStream(path.join(path.resolve(__dirname), '../README.md'))
        for await (const data of streamFS) { sourceData += data.toString('utf8') }
        let targetData = ''
        const aliceStream = await alice.getReadStream('./README.md', alice.driveAddress, key)
        for await (const data of aliceStream) { targetData += data.toString('utf8') }
        // establish size of drive before file removed
        const sizeBefore = await localUtil.totalSize(testStorage)
        // remove file
        await alice.remove('./README.md', alice.driveAddress)
        // establish size of drive after file removed
        const sizeAfter = await localUtil.totalSize(testStorage)
        // establish difference between size of drive before and after file removed
        const sizeDiff = sizeBefore - sizeAfter
        // check sizeDiff equals fileSize
        assert.equals(sizeDiff, readmeSize, 'file successfully removed')
        alice.close()
        await localUtil.rmTmpDir(testStorage)
        next()
    })

    context('on restart, reconnect to previously loaded remote drives', async (assert, next) => {
        const testStorageAlice = localUtil.makeTmpDir()
        const testStorageBob = localUtil.makeTmpDir()
        const key = ArtifactStore.artifactEncryptionKey()

        const alice = new ArtifactStore(testStorageAlice)
        await alice.ready()
        const bob = new ArtifactStore(testStorageBob)
        await bob.ready()

        const file1 = fs.createReadStream(path.join(path.resolve(__dirname), '../README.md'))
        const bobFile1 = bob.createWriteStream('./README.md', key)
        file1.pipe(bobFile1)
        await bob.update('./README.md')
        const file2 = fs.createReadStream(path.join(path.resolve(__dirname), './TESTFILE.md'))
        const bobFile2 = bob.createWriteStream('./TESTFILE.md', key)
        file2.pipe(bobFile2)
        await bob.update('./TESTFILE.md')

        const aliceGetFile1 = await alice.getReadStream('./README.md', bob.driveAddress, key)
        let aliceFile1 = ''
        for await (const data of aliceGetFile1) { aliceFile1 += data }
        const sourceFile1 = await readFile(path.join(path.resolve(__dirname), '../README.md'), 'utf8').catch((err) => { return undefined })
        assert.equal(aliceFile1, sourceFile1, 'alice is connected to bob and has replicated file1 from bob\'s hyperdrive')

        await alice.close()

        const aliceRestart = new ArtifactStore(testStorageAlice)
        await aliceRestart.ready()
        assert.ok(aliceRestart.otherDrives.has(bob.driveAddress.toString('hex')), 'after restart, alice automatically reconnects to bob')

        const aliceGetFile2 = await aliceRestart.getReadStream('./TESTFILE.md', bob.driveAddress, key)
        let aliceFile2 = ''
        for await (const data of aliceGetFile2) { aliceFile2 += data }
        const sourceFile2 = await readFile(path.join(path.resolve(__dirname), './TESTFILE.md'), 'utf8').catch((err) => { return undefined })
        assert.equal(aliceFile2, sourceFile2, 'alice is connected to bob and has replicated file2 from bob\'s hyperdrive')

        aliceRestart.close()
        bob.close()
        await localUtil.rmTmpDir(testStorageAlice)
        await localUtil.rmTmpDir(testStorageBob)
        next()
    })

    context('find the size of a given file on a drive', async (assert, next) => {
        const testStorageAlice = localUtil.makeTmpDir()
        const testStorageBob = localUtil.makeTmpDir()
        const key = ArtifactStore.artifactEncryptionKey()

        const alice = new ArtifactStore(testStorageAlice)
        await alice.ready()
        const bob = new ArtifactStore(testStorageBob)
        await bob.ready()

        const file1 = fs.createReadStream(path.join(path.resolve(__dirname), '../README.md'))
        const bobFile1 = bob.createWriteStream('./README.md', key)
        file1.pipe(bobFile1)
        await bob.update('./README.md')
        const originalFileStats = await stat(path.join(path.resolve(__dirname), '../README.md'))
        const originalFileSize = originalFileStats.size
        const driveFileSize = await alice.fileSize('README.md', bob.driveAddress)
        assert.equals(originalFileSize, driveFileSize, 'alice successfully determines the size of a file on bob\'s drive')

        alice.close()
        bob.close()
        await localUtil.rmTmpDir(testStorageAlice)
        await localUtil.rmTmpDir(testStorageBob)
        next()
    })
})
