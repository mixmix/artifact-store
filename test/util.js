const fs = require('fs')
const path = require('path')
const { promisify } = require('util')
const { tmpdir } = require('os')
const { randomBytes } = require('crypto')
const mkdirp = require('mkdirp').sync
const rimraf = require('rimraf')
const readdir = promisify(fs.readdir)
const stat = promisify(fs.stat)


// create a temporary directory for tests

function makeTmpDir() {
  const testStorage = path.join(tmpdir(), randomBytes(4).toString('hex'))
  mkdirp(testStorage)
  return testStorage
}

async function rmTmpDir(testStorage) {
  return new Promise((resolve, reject) => {
    rimraf(testStorage, (err) => {
      if (err) return reject(err)
      resolve()
    })
  })
}


// recursively walk a directory and output full paths of all files

async function* walk(baseDir) {
  if (baseDir[baseDir.length - 1] !== path.sep) baseDir = baseDir + path.sep
  yield* getFiles(baseDir)

  async function* getFiles(directory) {
    const files = await readdir(directory, { withFileTypes: true })

    for await (const f of files) {
      const fullPath = path.join(directory, f.name)
      if (f.isFile()) yield fullPath
      if (f.isDirectory()) yield* getFiles(fullPath)
    }
  }
}

// Using the above function, get the cumulative size of a directory

async function totalSize(pathToWalk) {
  let totalSize = 0
  for await (const filePath of walk(pathToWalk)) {
    const { size } = await stat(filePath)
    totalSize += size
  }
  return totalSize
}

module.exports = {
  makeTmpDir,
  rmTmpDir,
  walk,
  totalSize
}
