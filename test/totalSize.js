const { totalSize } = require('./util')
const path = require('path')

// This is just to demonstrate using the total size function
// we can delete this

const pathToWalk = path.join(path.resolve(__dirname), '..')

async function main () {
  const total = await totalSize(pathToWalk)
  console.log(`Total size of ${pathToWalk} is ${total} bytes`)
}

main()
